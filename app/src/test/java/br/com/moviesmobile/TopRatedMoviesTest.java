package br.com.moviesmobile;

import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.com.moviesmobile.config.Api;
import br.com.moviesmobile.config.RetrofitConfig;
import br.com.moviesmobile.model.Movie;
import br.com.moviesmobile.services.MoviesServices;
import br.com.moviesmobile.services.impl.MoviesServicesImpl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class TopRatedMoviesTest {

    @Test
    public void deveRetornarJsonValido(){

        MoviesServicesImpl moviesServices = new MoviesServicesImpl();
        Movie movies = moviesServices.getTopPopularMovies();

        assertNotNull(movies);


    }
}


