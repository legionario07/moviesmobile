package br.com.moviesmobile.config;

import android.os.Parcelable;

import java.io.Serializable;

public class Api implements Serializable {
    private Integer id;
    private String descricao;
    //Poderia ser solicitado pelo usuario via SQL ou SharedPrefs
    private String apiKey = "2d3679c08b33735a649387455d75a6b7";

    public Api(){

    }

    public Api(Integer id){
        this();
        this.id = id;
    }

    public Api(Integer id, String descricao, String apiKey){
        this();
        this.id = id;
        this.descricao = descricao;
        this.apiKey = apiKey;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
