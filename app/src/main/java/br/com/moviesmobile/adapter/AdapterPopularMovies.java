package br.com.moviesmobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.moviesmobile.R;
import br.com.moviesmobile.model.Result;

public class AdapterPopularMovies extends ArrayAdapter<Result> {

    private List<Result> movies;
    private Context context;

    public AdapterPopularMovies(Context context, List<Result> movies) {
        super(context, 0, movies);
        this.context = context;
        this.movies = movies;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Result movie = this.movies.get(position);

        convertView = LayoutInflater.from(this.context).inflate(R.layout.activity_item_popular, null);

        TextView txtID =  convertView.findViewById(R.id.txtId);
        TextView txtTitle =  convertView.findViewById(R.id.txtTitle);
        TextView txtVotes =  convertView.findViewById(R.id.txtVotes);
        TextView txtPopularity =  convertView.findViewById(R.id.txtPopularity);

        txtID.setText(movie.getId().toString());
        txtTitle.setText(movie.getTitle());
        txtVotes.setText(movie.getVoteAverage().toString());
        txtPopularity.setText(movie.getPopularity().toString());

        return convertView;
    }


}
