package br.com.moviesmobile.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Set;

import br.com.moviesmobile.R;
import br.com.moviesmobile.adapter.AdapterPopularMovies;
import br.com.moviesmobile.model.Movie;
import br.com.moviesmobile.model.Result;
import br.com.moviesmobile.services.impl.MoviesServicesImpl;
import br.com.moviesmobile.utils.ConstraintsUtil;
import br.com.moviesmobile.utils.VerificaConexaoStrategy;

public class MainActivity extends AppCompatActivity {

    private Movie movies;
    private Movie movieTemp;
    private ListView lstTopRated;
    private AdapterPopularMovies adapterTopRated;
    private MoviesServicesImpl moviesServices;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!VerificaConexaoStrategy.verificarConexao(this)) {
            Toast.makeText(this, "Verifique sua conexão com a Internet", Toast.LENGTH_LONG).show();
            return;
        }

        moviesServices = new MoviesServicesImpl();
        movies = moviesServices.getTopPopularMovies();

        searchView = findViewById(R.id.searchTitleMovie);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filterListView(s);
                return false;
            }
        });

        lstTopRated = findViewById(R.id.lstTopRated);

        lstTopRated.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Result result = (Result) lstTopRated.getItemAtPosition(position);
                Intent i = new Intent(getApplicationContext(), MovieActivity.class);
                i.putExtra(ConstraintsUtil.FLAG_EXTRAS_KEY, result);
                startActivity(i);

            }
        });

        lstTopRated.setTextFilterEnabled(true);

        getAllPopularMovies();
        createOrUpdateListView(movies);

        Toast.makeText(this, "Clique no filme para ver Detalhes",Toast.LENGTH_SHORT).show();

    }

    private void createOrUpdateListView(Movie movie) {

        adapterTopRated = new AdapterPopularMovies(this, movie.getResults());
        lstTopRated.setAdapter(adapterTopRated);
    }

    private void createOrUpdateListView() {

        if (adapterTopRated == null) {
            adapterTopRated = new AdapterPopularMovies(this, movies.getResults());
            lstTopRated.setAdapter(adapterTopRated);
        } else {
            adapterTopRated.notifyDataSetChanged();
        }
    }


    public void getAllPopularMovies() {

        int totalPages = movies.getTotalPages();
        if (totalPages > 10) {
            totalPages = 10;
        }
        for (int i = 2; i < totalPages; i++) {
            movies.getResults().addAll(moviesServices.getTopPopularMovies(String.valueOf(i)));
        }

    }

    private void filterListView(String text) {

        if (text.equals("")) {
            movieTemp = new Movie();
            createOrUpdateListView(movies);
        } else {

            if (movieTemp == null) {
                movieTemp = new Movie();
            }

            movieTemp.getResults().clear();
            Set<Result> results = new HashSet<>();
            for (int i = 0; i < movies.getResults().size(); i++) {
                if (text.length() <= movies.getResults().get(i).getTitle().length()) {
                    if (text.equalsIgnoreCase((String) movies.getResults().get(i).getTitle().subSequence(0, text.length()))) {
                        results.add(movies.getResults().get(i));
                    }
                }
            }

            for (Result r : results) {
                movieTemp.getResults().add(r);
            }
            createOrUpdateListView(movieTemp);


        }


    }
}
