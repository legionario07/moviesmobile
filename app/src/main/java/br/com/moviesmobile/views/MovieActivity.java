package br.com.moviesmobile.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import br.com.moviesmobile.R;
import br.com.moviesmobile.model.Result;
import br.com.moviesmobile.utils.ConstraintsUtil;

public class MovieActivity extends AppCompatActivity {

    private TextView txtTitle;
    private TextView txtPopularity;
    private TextView txtOriginalLanguage;
    private TextView txtOriginalTitle;
    private TextView txtCountVotes;
    private TextView txtAverageVotes;
    private TextView txtOverview;

    private Result resultMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        txtTitle = findViewById(R.id.txtTitle);
        txtPopularity = findViewById(R.id.txtPopularity);
        txtOriginalLanguage = findViewById(R.id.txtOriginalLanguage);
        txtOriginalTitle = findViewById(R.id.txtOriginalTitle);
        txtCountVotes = findViewById(R.id.txtVotesCounts);
        txtAverageVotes = findViewById(R.id.txtVotesAverage);
        txtOverview = findViewById(R.id.txtOverview);

        if(getIntent().getExtras().containsKey(ConstraintsUtil.FLAG_EXTRAS_KEY)){
            resultMovie = (Result) getIntent().getSerializableExtra(ConstraintsUtil.FLAG_EXTRAS_KEY);
        }else{
            Toast.makeText(this, "Filme desconhecido",Toast.LENGTH_LONG).show();
            onBackPressed();
        }

        fillObjectInView();


    }

    private void fillObjectInView() {

        txtTitle.setText(resultMovie.getTitle());
        txtPopularity.setText(resultMovie.getPopularity().toString());
        txtOriginalLanguage.setText(resultMovie.getOriginalLanguage());
        txtOriginalTitle.setText(resultMovie.getOriginalTitle());
        txtCountVotes.setText(resultMovie.getVoteCount().toString());
        txtAverageVotes.setText(resultMovie.getVoteAverage().toString());
        txtOverview.setText(resultMovie.getOverview());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
