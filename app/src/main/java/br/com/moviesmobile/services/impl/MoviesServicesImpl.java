package br.com.moviesmobile.services.impl;

import android.os.Handler;

import java.io.IOException;
import java.util.List;

import br.com.moviesmobile.config.Api;
import br.com.moviesmobile.config.RetrofitConfig;
import br.com.moviesmobile.model.Movie;
import br.com.moviesmobile.model.Result;
import br.com.moviesmobile.services.MoviesServices;
import retrofit2.Retrofit;

public class MoviesServicesImpl {

    private Retrofit retrofit;
    private MoviesServices service;
    private Movie topPopularMovies;
    private Api api = new Api();
    private Handler handler = new Handler();

    public Movie getTopPopularMovies(){

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                retrofit = RetrofitConfig.getBuilder();
                service = retrofit.create(MoviesServices.class);
                try {
                    topPopularMovies = service.getPopularMovies(api.getApiKey()).execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return topPopularMovies;

    }

    public List<Result> getTopPopularMovies(final String page){

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                retrofit = RetrofitConfig.getBuilder();
                service = retrofit.create(MoviesServices.class);
                try {
                    topPopularMovies = service.getPopularMovies(api.getApiKey(), page).execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        t.start();

        return topPopularMovies.getResults();

    }


}
