package br.com.moviesmobile.services;

import java.util.List;

import br.com.moviesmobile.model.Movie;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoviesServices {

    String BASE = "movie/";

    @GET(BASE + "popular")
    Call<Movie> getPopularMovies(@Query("api_key") String apiKey);

    @GET(BASE + "popular")
    Call<Movie> getPopularMovies(@Query("api_key") String apiKey, @Query("page") String page);


}


